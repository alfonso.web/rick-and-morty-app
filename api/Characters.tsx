export async function getCharacters(nextUrl) {
  try {
    const url = "https://rickandmortyapi.com/api/character";
    const response = await fetch(nextUrl || url);
    const result = await response.json();
    return result;
  } catch (error) {
    throw error;
  }
}

export async function showCharacter(url) {
  try {
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    throw error;
  }
}
