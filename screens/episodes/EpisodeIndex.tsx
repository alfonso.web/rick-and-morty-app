import React, { useState, useCallback } from "react";
import { View, FlatList, StyleSheet, TouchableOpacity } from "react-native";
import { getEpisodes } from "../../api/Episodes";
import { ListItem } from "react-native-elements";
import { useNavigation, useFocusEffect } from "@react-navigation/native";

export default function EpisodeIndex() {
  const [episodes, setEpisodes] = useState([]);
  const navigation = useNavigation();

  useFocusEffect(
    useCallback(() => {
      let isActive = true;
      (async () => {
        const loadEpisodes = async () => {
          try {
            const response = await getEpisodes();
            if (isActive) {
              setEpisodes(response.results);
            }
          } catch (error) {
            navigation.goBack();
          }
        };
        loadEpisodes();
      })();
      return () => {
        isActive = false;
      };
    }, [navigation])
  );

  const showEpisode = (props) => {
    const { url, name, navigation } = props;
    navigation.navigate("episode-show", { url: url, name: name });
  };
  const renderItem = ({ item }) => (
    <TouchableOpacity
      onPress={() =>
        showEpisode({
          url: item.url,
          name: item.name,
          navigation,
        })
      }
    >
      <ListItem bottomDivider containerStyle={styles.listItem}>
        <ListItem.Content>
          <ListItem.Title style={styles.title}>{item.name}</ListItem.Title>
          <ListItem.Subtitle style={styles.subtitle}>
            {item.episode}
          </ListItem.Subtitle>
        </ListItem.Content>
        <ListItem.Chevron />
      </ListItem>
    </TouchableOpacity>
  );
  return (
    <View style={styles.fullContainer}>
      <FlatList
        data={episodes}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  fullContainer: {
    backgroundColor: "#FDFEFE",
  },
  listItem: {
    paddingTop: 20,
    paddingBottom: 20,
  },
  title: {
    color: "#696969",
    fontWeight: "bold",
  },
  subtitle: {
    color: "#A9A9A9",
    fontWeight: "bold",
  },
});
