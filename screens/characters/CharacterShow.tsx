import React, { useState, useCallback } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
} from "react-native";
import { showCharacter } from "../../api/Characters";
import { ListItem, Avatar } from "react-native-elements";
import { useFocusEffect } from "@react-navigation/native";
import { showLocation } from "../../api/Locations";
import Loading from "../../components/Loading";
import { showEpisode } from "../../api/Episodes";
import { useNavigation } from "@react-navigation/native";

export default function CharacterShow(props) {
  const { route } = props;
  const { url, name } = route.params;
  const [character, setCharacter] = useState({});
  const [location, setLocation] = useState({});
  const [episode, setEpisode] = useState([]);
  const [loading, setLoading] = useState(false);
  const navigation = useNavigation();

  React.useLayoutEffect(() => {
    navigation.setOptions({ title: name });
  }, [navigation, name]);

  useFocusEffect(
    useCallback(() => {
      let isActive = true;
      (async () => {
        setLoading(true);
        const getCharacter = async () => {
          try {
            const episodesArray = [];
            const responseCharacter = await showCharacter(url);
            if (isActive) {
              setCharacter(responseCharacter);
              const responseLocation = await showLocation(
                responseCharacter.location.url
              );
              setLocation(responseLocation);

              for await (const value of responseCharacter.episode) {
                const responseEpisosde = await showEpisode(value);

                episodesArray.push({
                  id: responseEpisosde.id,
                  name: responseEpisosde.name,
                  episode: responseEpisosde.episode,
                  url: responseEpisosde.url,
                });
              }
              setEpisode(episodesArray);
            }
            setLoading(false);
          } catch (error) {
            navigation.goBack();
          }
        };
        getCharacter();
      })();
      return () => {
        isActive = false;
      };
    }, [url, navigation])
  );

  const goEpisode = (props) => {
    const { url, name, navigation } = props;
    navigation.navigate("tab-episodes", {
      screen: "episode-show",
      params: { url, name },
    });
  };

  const renderItem = ({ item }) => (
    <TouchableOpacity
      onPress={() =>
        goEpisode({
          url: item.url,
          name: item.name,
          navigation,
        })
      }
    >
      <ListItem bottomDivider>
        <ListItem.Content>
          <ListItem.Title style={styles.title}>{item.name}</ListItem.Title>
          <ListItem.Subtitle style={styles.subtitle}>
            {item.episode}
          </ListItem.Subtitle>
        </ListItem.Content>
        <ListItem.Chevron />
      </ListItem>
    </TouchableOpacity>
  );

  return (
    <>
      {!loading && (
        <View style={styles.container}>
          <View style={styles.containerAvatar}>
            <Avatar rounded size="xlarge" source={{ uri: character.image }} />
            <Text style={styles.title}>{character.name}</Text>
          </View>
          <View style={styles.containerInfo}>
            <View>
              <Text style={styles.title}>Species</Text>
              <Text style={styles.subtitle}>{character.species}</Text>
            </View>
            <View>
              <Text style={styles.title}>Gender</Text>
              <Text style={styles.subtitle}>{character.gender}</Text>
            </View>
            <View>
              <Text style={styles.title}>Created</Text>
              <Text style={styles.subtitle}>{character.created}</Text>
            </View>
          </View>
          <View style={styles.containerLocation}>
            <Text style={styles.title}>Location</Text>
            <TouchableOpacity>
              <ListItem key={location.id} bottomDivider>
                <ListItem.Content>
                  <ListItem.Title style={styles.title}>
                    {location.name}
                  </ListItem.Title>
                  <ListItem.Subtitle style={styles.subtitle}>
                    {location.type}
                  </ListItem.Subtitle>
                </ListItem.Content>
                <ListItem.Chevron />
              </ListItem>
            </TouchableOpacity>
          </View>
          <View style={styles.containerEpisode}>
            <Text style={styles.title}>Episodes</Text>
            <FlatList
              data={episode}
              renderItem={renderItem}
              keyExtractor={(item) => item.id}
            />
          </View>
        </View>
      )}
      <Loading isVisible={loading} text="Loading..." />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#FDFEFE",
    flexDirection: "column",
    paddingLeft: 15,
    paddingRight: 15,
  },

  containerAvatar: {
    paddingTop: 20,
    paddingBottom: 20,
    alignItems: "center",
  },

  containerInfo: {
    paddingTop: 20,
    flexDirection: "row",
    justifyContent: "space-around",
  },
  title: {
    color: "#696969",
    fontWeight: "bold",
  },
  subtitle: {
    color: "#A9A9A9",
    fontWeight: "bold",
  },
  containerLocation: {
    paddingTop: 20,
  },
  containerEpisode: {
    paddingTop: 20,
  },
});
