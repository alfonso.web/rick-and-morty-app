import React, { useState, useCallback } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
} from "react-native";
import { useFocusEffect } from "@react-navigation/native";
import { showEpisode } from "../../api/Episodes";
import { showCharacter } from "../../api/Characters";
import Loading from "../../components/Loading";
import { ListItem } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";

export default function EpisodeShow(props) {
  const { route } = props;
  const { url, name } = route.params;
  const [episode, setEpisode] = useState({});
  const [character, setCharacter] = useState([]);
  const [loading, setLoading] = useState(false);
  const navigation = useNavigation();

  React.useLayoutEffect(() => {
    navigation.setOptions({ title: name });
  }, [navigation, name]);

  useFocusEffect(
    useCallback(() => {
      let isActive = true;
      (async () => {
        setLoading(true);
        const characterArray = [];

        const getEpisode = async () => {
          try {
            const responseEpisode = await showEpisode(url);

            if (isActive) {
              setEpisode(responseEpisode);
              for await (const value of responseEpisode.characters) {
                const responseCharacter = await showCharacter(value);

                characterArray.push({
                  id: responseCharacter.id,
                  name: responseCharacter.name,
                  gender: responseCharacter.gender,
                  url: responseCharacter.url,
                });
              }
              setCharacter(characterArray);
            }
            setLoading(false);
          } catch (e) {
            navigation.goBack();
          }
        };
        getEpisode();
      })();
      return () => {
        isActive = false;
      };
    }, [url, navigation])
  );

  const goCharacter = (props) => {
    const { url, name, navigation } = props;
    navigation.navigate("character-show", { url: url, name: name });
  };

  const renderItem = ({ item }) => (
    <TouchableOpacity
      onPress={() =>
        goCharacter({
          url: item.url,
          name: item.name,
          navigation,
        })
      }
    >
      <ListItem bottomDivider>
        <ListItem.Content>
          <ListItem.Title style={styles.title}>{item.name}</ListItem.Title>
          <ListItem.Subtitle style={styles.subtitle}>
            {item.gender}
          </ListItem.Subtitle>
        </ListItem.Content>
        <ListItem.Chevron />
      </ListItem>
    </TouchableOpacity>
  );

  return (
    <>
      {!loading && (
        <View style={styles.container}>
          <View style={styles.containerParagraph}>
            <View>
              <Text style={styles.title}>Name</Text>
              <Text style={styles.subtitle}>{episode.name}</Text>
            </View>
            <View>
              <Text style={styles.title}>Episode</Text>
              <Text style={styles.subtitle}>{episode.episode}</Text>
            </View>
          </View>
          <View style={styles.containerParagraph}>
            <View>
              <Text style={styles.title}>Air date</Text>
              <Text style={styles.subtitle}>{episode.air_date}</Text>
            </View>
            <View>
              <Text style={styles.title}>Created</Text>
              <Text style={styles.subtitle}>{episode.created}</Text>
            </View>
          </View>
          <View style={styles.containerCharacter}>
            <Text style={styles.title}>Characters</Text>
            <FlatList
              data={character}
              renderItem={renderItem}
              keyExtractor={(item) => item.id}
            />
          </View>
        </View>
      )}
      <Loading isVisible={loading} text="Loading..." />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#FDFEFE",
    flexDirection: "column",
    paddingLeft: 15,
    paddingRight: 15,
  },
  containerParagraph: {
    flexDirection: "row",
    justifyContent: "space-around",
    paddingTop: 20,
    paddingBottom: 20,
  },
  title: {
    color: "#696969",
    fontWeight: "bold",
  },
  subtitle: {
    color: "#A9A9A9",
    fontWeight: "bold",
  },
  containerCharacter: {
    paddingTop: 20,
  },
});
