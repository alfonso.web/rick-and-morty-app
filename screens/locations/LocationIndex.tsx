import React, { useState, useCallback } from "react";
import { View, FlatList, StyleSheet, TouchableOpacity } from "react-native";
import { getLocations } from "../../api/Locations";
import { ListItem } from "react-native-elements";

import { useNavigation, useFocusEffect } from "@react-navigation/native";

export default function LocationIndex() {
  const [locations, setLocations] = useState([]);
  const navigation = useNavigation();

  useFocusEffect(
    useCallback(() => {
      let isActive = true;
      (async () => {
        const loadLocations = async () => {
          try {
            const response = await getLocations();
            if (isActive) {
              setLocations(response.results);
            }
          } catch (error) {
            navigation.goBack();
          }
        };
        loadLocations();
      })();
      return () => {
        isActive = false;
      };
    }, [navigation])
  );

  const showLocation = (props) => {
    const { url, name, navigation } = props;
    navigation.navigate("location-show", { url: url, name: name });
  };
  const renderItem = ({ item }) => (
    <TouchableOpacity
      onPress={() =>
        showLocation({
          url: item.url,
          name: item.name,
          navigation,
        })
      }
    >
      <ListItem bottomDivider containerStyle={styles.listItem}>
        <ListItem.Content>
          <ListItem.Title style={styles.title}>{item.name}</ListItem.Title>
          <ListItem.Subtitle style={styles.subtitle}>
            {item.type}
          </ListItem.Subtitle>
        </ListItem.Content>
        <ListItem.Chevron />
      </ListItem>
    </TouchableOpacity>
  );

  return (
    <View style={styles.fullContainer}>
      <FlatList
        data={locations}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  fullContainer: {
    backgroundColor: "#FDFEFE",
  },
  listItem: {
    paddingTop: 20,
    paddingBottom: 20,
  },
  title: {
    color: "#696969",
    fontWeight: "bold",
  },
  subtitle: {
    color: "#A9A9A9",
    fontWeight: "bold",
  },
});
