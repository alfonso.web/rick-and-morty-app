import React, { useState, useCallback } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
} from "react-native";
import { showLocation } from "../../api/Locations";
import { showCharacter } from "../../api/Characters";
import Loading from "../../components/Loading";
import { ListItem } from "react-native-elements";
import { useNavigation, useFocusEffect } from "@react-navigation/native";

export default function LocationShow(props) {
  const [location, setLocation] = useState({});
  const [character, setCharacter] = useState([]);
  const [loading, setLoading] = useState(false);
  const navigation = useNavigation();

  const { route } = props;
  const { url, name } = route.params;

  React.useLayoutEffect(() => {
    navigation.setOptions({ title: name });
  }, [navigation, name]);

  useFocusEffect(
    useCallback(() => {
      let isActive = true;
      (async () => {
        setLoading(true);
        const getLocation = async () => {
          try {
            const characterArray = [];
            const responseLocation = await showLocation(url);
            if (isActive) {
              setLocation(responseLocation);
              for await (const value of responseLocation.residents) {
                const responseCharacter = await showCharacter(value);

                characterArray.push({
                  id: responseCharacter.id,
                  name: responseCharacter.name,
                  gender: responseCharacter.gender,
                  url: responseCharacter.url,
                });
              }
              setCharacter(characterArray);
            }
            setLoading(false);
          } catch (error) {
            navigation.goBack();
          }
        };
        getLocation();
      })();
      return () => {
        isActive = false;
      };
    }, [url, navigation])
  );

  const goCharacter = (props) => {
    const { url, name, navigation } = props;
    navigation.navigate("character-show", { url: url, name: name });
  };

  const renderItem = ({ item }) => (
    <TouchableOpacity
      onPress={() =>
        goCharacter({
          url: item.url,
          name: item.name,
          navigation,
        })
      }
    >
      <ListItem bottomDivider>
        <ListItem.Content>
          <ListItem.Title style={styles.title}>{item.name}</ListItem.Title>
          <ListItem.Subtitle style={styles.subtitle}>
            {item.gender}
          </ListItem.Subtitle>
        </ListItem.Content>
        <ListItem.Chevron />
      </ListItem>
    </TouchableOpacity>
  );

  return (
    <>
      {!loading && (
        <View style={styles.container}>
          <View style={styles.containerParagraph}>
            <View>
              <Text style={styles.title}>Name</Text>
              <Text style={styles.subtitle}>{location.name}</Text>
            </View>
            <View>
              <Text style={styles.title}>Type</Text>
              <Text style={styles.subtitle}>{location.type}</Text>
            </View>
          </View>
          <View style={styles.containerParagraph}>
            <View>
              <Text style={styles.title}>Dimension</Text>
              <Text style={styles.subtitle}>{location.dimension}</Text>
            </View>
            <View>
              <Text style={styles.title}>Created</Text>
              <Text style={styles.subtitle}>{location.created}</Text>
            </View>
          </View>
          <View style={styles.containerCharacter}>
            <Text style={styles.title}>Residents</Text>
            <FlatList
              data={character}
              renderItem={renderItem}
              keyExtractor={(item) => item.id}
            />
          </View>
        </View>
      )}
      <Loading isVisible={loading} text="Loading..." />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#FDFEFE",
    flexDirection: "column",
    paddingLeft: 15,
    paddingRight: 15,
  },
  containerParagraph: {
    flexDirection: "row",
    justifyContent: "space-around",
    paddingTop: 20,
    paddingBottom: 20,
  },
  title: {
    color: "#696969",
    fontWeight: "bold",
  },
  subtitle: {
    color: "#A9A9A9",
    fontWeight: "bold",
  },
  containerCharacter: {
    paddingTop: 20,
  },
});
