import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import CharacterIndex from "../screens/characters/CharacterIndex";
import CharacterShow from "../screens/characters/CharacterShow";

const Stack = createNativeStackNavigator();
export default function CharactersStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="characters"
        component={CharacterIndex}
        options={{ title: "Characters" }}
      />
      <Stack.Screen
        name="character-show"
        component={CharacterShow}
        options={{ title: "" }}
      />
    </Stack.Navigator>
  );
}
