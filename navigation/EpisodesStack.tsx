import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import EpisodeIndex from "../screens/episodes/EpisodeIndex";
import EpisodeShow from "../screens/episodes/EpisodeShow";
import { useNavigation } from "@react-navigation/native";

const Stack = createNativeStackNavigator();

export default function EpisodesStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="episodes"
        component={EpisodeIndex}
        options={{ title: "Episodes" }}
      />
      <Stack.Screen
        name="episode-show"
        component={EpisodeShow}
        options={{
          title: "",
        }}
      />
    </Stack.Navigator>
  );
}
