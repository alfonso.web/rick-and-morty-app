import React, { useState, useEffect } from "react";
import {
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import { ListItem, Avatar } from "react-native-elements";
import { getCharacters } from "../../api/Characters";
import { useNavigation, useFocusEffect } from "@react-navigation/native";

export default function CharactersIndex() {
  const [characters, setCharacters] = useState([]);
  const [nextUrl, setNextUrl] = useState(null);
  const navigation = useNavigation();

  useEffect(() => {
    (async () => {
      await loadCharacters();
    })();
  }, []);

  const loadCharacters = async () => {
    try {
      const response = await getCharacters(nextUrl);
      setNextUrl(response.info.next);
      setCharacters(response.results);
    } catch (error) {
      navigation.goBack();
    }
  };

  const showCharacter = (props) => {
    const { url, name, navigation } = props;
    navigation.navigate("character-show", { url: url, name: name });
  };

  const renderItem = ({ item }) => (
    <TouchableOpacity
      onPress={() =>
        showCharacter({
          url: item.url,
          name: item.name,
          navigation,
        })
      }
    >
      <ListItem bottomDivider containerStyle={styles.listItem}>
        <Avatar rounded size="medium" source={{ uri: item.image }} />
        <ListItem.Content>
          <ListItem.Title style={styles.title}>{item.name}</ListItem.Title>
          <ListItem.Subtitle style={styles.subtitle}>
            {item.gender}
          </ListItem.Subtitle>
        </ListItem.Content>
        <ListItem.Chevron />
      </ListItem>
    </TouchableOpacity>
  );
  return (
    <View style={styles.fullContainer}>
      <FlatList
        data={characters}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        onEndReached={nextUrl && loadCharacters}
        onEndReachedThreshold={0.1}
        showsVerticalScrollIndicator={true}
        ListFooterComponent={
          nextUrl && (
            <ActivityIndicator
              size="large"
              // style={styles.spinner}
              color="#AEAEAE"
            />
          )
        }
      />
    </View>
  );
}

const styles = StyleSheet.create({
  fullContainer: {
    backgroundColor: "#FDFEFE",
  },

  listItem: {
    paddingTop: 20,
    paddingBottom: 20,
  },
  title: {
    color: "#696969",
    fontWeight: "bold",
  },
  subtitle: {
    color: "#A9A9A9",
    fontWeight: "bold",
  },
});
