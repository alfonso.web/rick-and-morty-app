import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
} from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import CharactersStack from "./CharactersStack";
import LocationsStack from "./LocationsStack";
import EpisodesStack from "./EpisodesStack";
import { Icon } from "react-native-elements";
import { useColorScheme } from "react-native";

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: "rgb(255, 45, 85)",
  },
};
export default function Navigation() {
  const scheme = useColorScheme();

  return (
    <NavigationContainer theme={scheme === "dark" ? DarkTheme : DefaultTheme}>
      <RootNavigation />
    </NavigationContainer>
  );
}

const RootNavigation = () => {
  const Stack = createNativeStackNavigator();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Root"
        component={BottomTabNavigator}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

const BottomTabNavigator = () => {
  const BottomTab = createBottomTabNavigator();
  return (
    <BottomTab.Navigator>
      <BottomTab.Screen
        name="tab-characters"
        component={CharactersStack}
        options={{
          headerShown: false,
          tabBarLabel: "Characters",
          tabBarIcon: () => <Icon name="list-outline" type="ionicon" />,
        }}
      />
      <BottomTab.Screen
        name="tab-locations"
        component={LocationsStack}
        options={{
          headerShown: false,
          tabBarLabel: "Locations",
          tabBarIcon: () => <Icon name="list-outline" type="ionicon" />,
        }}
      />
      <BottomTab.Screen
        name="tab-episodes"
        component={EpisodesStack}
        options={{
          headerShown: false,
          tabBarLabel: "Episodes",
          tabBarIcon: () => <Icon name="list-outline" type="ionicon" />,
        }}
      />
    </BottomTab.Navigator>
  );
};
