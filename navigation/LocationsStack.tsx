import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import LocationIndex from "../screens/locations/LocationIndex";
import LocationShow from "../screens/locations/LocationShow";

const Stack = createNativeStackNavigator();

export default function LocationsStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="locations"
        component={LocationIndex}
        options={{ title: "Locations" }}
      />
      <Stack.Screen
        name="location-show"
        component={LocationShow}
        options={{ title: "" }}
      />
    </Stack.Navigator>
  );
}
